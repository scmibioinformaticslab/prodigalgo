from os import listdir, system
from os.path import isfile, join

class prodigalGo:
	#################################################################
	#####                                                       #####
	#####                   CLASS PROPERTIES                    #####
	#####                                                       #####
	#################################################################
	# @subjectFullPath[string]: full path to the folder containing subject
	#							or reference files
	# @queryFullPath[string]: full path to the folder containing input query files
	# @outputFullPath[string]: full path to the folder to contain output files
	# @subjectFilenameList[list]: list of all subject file names
	# @queryFilenameList[list]: list of all query file names
	# @outputFilenameList[list]: list of all output file names

	subjectFullPath = ''
	queryFullPath = ''
	outputFullPath = ''
	subjectFilenameList = []
	queryFilenameList = []
	outputFilenameList = []

	# @programPath[string]: full path to the folder containing PRODIGAL binary file
	# @programParameterString[string]: optional parameter string

	programPath = ''
	programParameterString = ''
	#################################################################


	#################################################################
	#####                                                       #####
	#####                   CLASS CONSTRUCTOR                   #####
	#####                                                       #####
	#################################################################
	# Initiate class with required parameters and load all filenames
	# ========================== INPUT ==============================
	# @subjectFullPath[string]: full path to the folder containing subject
	#							or reference files
	# @queryFullPath[string]: full path to the folder containing input query files
	# @outputFullPath[string]: full path to the folder to contain output files
	# @programPath[string]: full path to the folder containing PRODIGAL binary file
	# @programParameterString[string]: optional parameter string
	#################################################################
	def __init__(self, subjectFullPath = '', queryFullPath = '', outputFullPath = '', programPath = '', programParameterString = ''):
		# assigning class data
		self.subjectFullPath = subjectFullPath
		self.queryFullPath = queryFullPath
		self.outputFullPath = outputFullPath
		self.programPath = programPath
		self.programParameterString = programParameterString

		# check if paths ends with '/'
		# if not, add '/' to path data
		if subjectFullPath and not subjectFullPath[-1] == '/':
			self.subjectFullPath += '/'
		if queryFullPath and not queryFullPath[-1] == '/':
			self.queryFullPath += '/'
		if outputFullPath and not outputFullPath[-1] == '/':
			self.outputFullPath += '/'
		if programPath and not programPath[-1] == '/':
			self.programPath += '/'

		# load list of subject and query filenames into class
		self.loadFilenameList('subject')
		self.loadFilenameList('query')

	######################### CLASS METHOD ##########################
	#####                   loadFilenameList                    #####
	#################################################################
	# Load the indicated filename list into class properties
	# ========================== INPUT ==============================
	# @list_indicator[string]: indicate which list of file to be load
	#								'subject', 'query', or 'output'
	#################################################################
	def loadFilenameList(self, list_indicator = False):
		# ==================== LOCAL VARIABLES ======================
		# @f[file]: temporary variable to keep each filename while checking
		# ===========================================================

		# printing progress to terminal
		if list_indicator == False or list_indicator == 'subject':
			print 'Loading file list from Subject input folder.'
			# list everything from @subjectFullPath and append to @ subjectFilenameList if it is a file
			self.subjectFilenameList = [ f for f in listdir(self.subjectFullPath) if isfile(join(self.subjectFullPath, f)) ]

		if list_indicator == False or list_indicator == 'query':
			print 'Loading file list from Query input folder.'
			# list everything from @queryFullPath and append to @ queryFilenameList if it is a file
			self.queryFilenameList = [ f for f in listdir(self.queryFullPath) if isfile(join(self.queryFullPath, f)) ]

		if list_indicator == False or list_indicator == 'output':
			print 'Loading file list from output folder.'
			# list everything from @outputFullPath and append to @ outputFilenameList if it is a file
			self.outputFilenameList = [ f for f in listdir(self.outputFullPath) if isfile(join(self.outputFullPath, f)) ]

	######################### CLASS METHOD ##########################
	#####                      runProgram                       #####
	#################################################################
	# Run PRODIGAL from the reference and input files indicated
	# ========================== INPUT ==============================
	# @outputNucleotidePath[string]: (optional) path which nucleotide CDS will be written to
	# @outputProteinPath[string]: (optional) path which protein CDS will be written to
	# ========================= OUTPUT ==============================
	# the method run PRODIGAL program and generate output to file system
	#################################################################
	def runProgram(self, outputNucleotidePath = False, outputProteinPath = False):
		# Reload output filenames first
		self.loadFilenameList('output')

		# Loop through each input file names
		for a_queryname in self.queryFilenameList:
			# ==================== LOCAL VARIABLES ======================
			# @a_queryname[string]: a temp for each input file
			# @query_id[string]: the id indicating the inputfile
			# @exists_output[list]: list of the output file already generated from the list of input
			# @output_name[string]: a temporary used in the process of checking the existing output
			# @output_nucleotide[string]: command string to run out the nucleotide output
			# @output_protein[string]: command string to run out the protein output
			# @command[string]: PRODIGAL command to be run to the system
			# ===========================================================
			# Get the specific id for the input name
			query_id = a_queryname[ :a_queryname.find('.') ]

			# Check for the already existing output generated from the input files
			exists_output = [output_name for output_name in self.outputFilenameList if query_id in output_name]
			# Don't run PRODIGAL on this one anymore if the output are already generated
			if exists_output:
				continue

			# Initiate @output_nucleotide and @output_protein to be insert to prodigal command
			output_nucleotide = ''
			output_protein = ''

			# If user indicate @outputNucleotidePath then add the command to run output nucleotide
			if outputNucleotidePath:
				# Add '/' to the path in case not already added
				if outputNucleotidePath[-1] != '/':
					outputNucleotidePath += '/'
				# construct @output_nucleotide
				output_nucleotide = '-d ' + outputNucleotidePath + query_id + '.coding_nucleotide.fa'

			# If user indicate @outputProteinPath then add the command to run output protein
			if outputProteinPath:
				# Add '/' to the path in case not already added
				if outputProteinPath[-1] != '/':
					outputProteinPath += '/'
				# construct @output_protein
				output_protein = '-a ' + outputProteinPath + query_id + '.coding_protein.fa'

			# Construct string command to run PRODIGAL in the system and run the command
			command = '%sprodigal -i %s -o %s %s %s' % (self.programPath, join(self.queryFullPath, a_queryname), join(self.outputFullPath, query_id + '.prodigal_out'), output_nucleotide, output_protein)
			system(command)

		# Load output filename list from the command to the system
		self.loadFilenameList('output')

	######################### CLASS METHOD ##########################
	#####                      parseOutput                      #####
	#################################################################
	# Open the output files, parse, and return results from PRODIGAL
	# ========================== INPUT ==============================
	# @firstToParse[int]: index in @outputFilenameList to start the parsing
	# @lastToParse[int]: index in @outputFilenameList to parse the last
	# ========================= OUTPUT ==============================
	# @result_dict[dictionary]: contain the results from all output
	#							from @firstToParse to @lastToParse
	#							structured as follow:
	#								key[string] - ID indicating the output
	#								value[list] - list of list containing
	#											  each PRODIGAL result
	#################################################################
	def parseOutput(self, firstToParse = False, lastToParse = False):
		# ==================== LOCAL VARIABLES ======================
		# @parsing_filename_list[list]: 
		# ===========================================================
		# Initiate @result_dict to contain the parsed result
		result_dict = {}

		# Get the list of files to parse from @outputFilenameList
		if firstToParse and lastToParse:
			parsing_filename_list = self.outputFilenameList[firstToParse:lastToParse + 1]
		elif firstToParse and not lastToParse:
			parsing_filename_list = self.outputFilenameList[firstToParse:]
		elif not firstToParse and lastToParse:
			parsing_filename_list = self.outputFilenameList[:lastToParse + 1]
		else:
			parsing_filename_list = self.outputFilenameList

		# Print the message to console
		print 'Parsing:\n',
		# Loop through each output file and parse to dictionary
		for an_output in parsing_filename_list:
			# ================== LOCAL VARIABLES ====================
			# @an_output[string]: a temp for each of the output file name
			# @output_id[string]: ID of the sequence parsed from output file
			# =======================================================
			# Get the ID indicating the sequence
			output_id = an_output[ : an_output.find('.')]
			# Initiate a list to contain parsed result
			result_dict[output_id] = []

			# Print message to colsole
			print output_id, '..',

			# Open the output file and parse its content
			with open(join(self.outputFullPath, an_output), 'r') as output_file:
				# ================ LOCAL VARIABLES ==================
				# @output_file[file]: file object contain information of the file to parse
				# @gene_count[int]: contain number of ORFs found in @output_file
				# ===================================================
				# Initialize @gene_count
				gene_count = 0

				# Loop through each line of the output file and parse content
				for a_line in output_file:
					# ============== LOCAL VARIABLES ================
					# @a_line[string/list]: a temp to contain each line of the @output_file
					# @position[list]: at [0] contain ORF starting position
					#				      [1] contain ORF ending position
					# ===============================================
					# If the line startswith space character then it would contain no information
					if not a_line.startswith(' '):
						# Just skip the line
						continue

					# Strip out newlines and split line with space
					a_line = a_line.replace('\n', '').split(' ')
					# Keep only the column that contain information
					a_line = [a_column for a_column in a_line if a_column]

					# If the list does not starts with '/' then it is the line with ORF positions
					if not a_line[0].startswith('/'):
						# ============ LOCAL VARIABLES ==============
						# @append_list[list]: at [0] contain given number ID of gene
						#				         [1] contain ORF starting position
						#				         [2] contain ORF ending position
						#				         [3] contain ORF reading frame direction
						# ===========================================
						# Initialize @append_list
						append_list = ['','','','']
						# Add current @gene_count as number ID of gene
						append_list[0] = gene_count

						# Replace '<' and '>' that might occur in the result
						a_line[1] = a_line[1].replace('<', '')
						a_line[1] = a_line[1].replace('>', '')

						# Check if the ORF is reverse complement or not
						if not a_line[1].startswith('complement'):
							# If the ORF is not reverse complement
							# Just split the start, end position
							position = a_line[1].split('..')
							# And add '+' as ORF direction
							append_list[3] = '+'
						else:
							# If the ORF is reverse complement
							# Find the position within the "( )" and split start, end position
							position = a_line[1][ a_line[1].find('(') + 1 : a_line[1].find(')')].split('..')
							# And add '-' as ORF direction
							append_list[3] = '-'

						# Put ORF starting and ending position into @append_list
						append_list[1] = position[0]
						append_list[2] = position[1]

						# Add @append_list contain the information into the @result_dict
						result_dict[output_id].append(append_list)
						# Add 1 to gene count
						gene_count += 1
		print '\n'

		return result_dict